app.component('product-display',{
  props: {
    premium: {
      type: Boolean,
      required: true
    }
  },
  template:
  `<div class="product-display">
      <div class="product-container">
        <div class="product-image">
          <img :src="image">
        </div>
        <div class="product-info">
          <h1>{{ title }}</h1>
          <p><strong>Details</strong></p>
          <ul>
            <li v-for="detail in details">{{ detail }}</li>
          </ul>
          <div class="color-circle" :style="{ backgroundColor: variant.color }"
          v-for="(variant, index) in variants" :key="variant.id" @mouseover="updateVariant(index)"></div>
          <p v-if = "inStock">In Stock</p>
          <p v-else>Out of Stock</p>
          <p>Shipping: {{ shipping }}</p>
          <button class="button" :class="{ disabledButton: !inStock }" :disabled="!inStock" @click="addToCart">Add to Cart</button>
        </div>
      </div>
      <review-list v-show="reviews.length" :reviews='reviews'></review-list>
      <review-form @review-submitted="addReview"></review-form>
    </div>`,
    data(){
      return{
        brand: 'Vue Learning',
        product: 'Socks',
        details: ['50% cotton', '30% wool', '20% polyester'],
        selectedVariant: 0,
        variants: [
          { id: 19017, color: 'Blue', image: './assets/images/blue_socks.jpg', quantity: 30},
          { id: 22010, color: 'Green', image: './assets/images/green_socks.jpg', quantity: 0 }
        ],
        reviews: []
      }
    },
    methods: {
      addToCart() {
        this.$emit('add-to-cart', this.variants[this.selectedVariant].id)
      },
      updateVariant(index) {
        this.selectedVariant = index
      },
      addReview(review){
        this.reviews.push(review)
      }
    },
    computed: {
      title() {
        return this.brand + ' ' + this.product
      },
      image() {
        return this.variants[this.selectedVariant].image
      },
      inStock() {
        return this.variants[this.selectedVariant].quantity
      },
      shipping() {
        if (this.premium) {
          return 'Free'
        }
        return '60TK'
      }
    }
})
